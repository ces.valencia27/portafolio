//
//  SchoolCell.swift
//  Portafolio
//
//  Created by David Valencia on 9/11/19.
//  Copyright © 2019 David. All rights reserved.
//

import UIKit

class SchoolCell: UICollectionViewCell {
    
    var modelView: SchoolViewModel? {
        didSet {
            if let viewModel = modelView {
                bgImage.image = UIImage(named: viewModel.getImageName + "Icon")
                schoolLabel.text = viewModel.getName
                studyLabel.text = viewModel.getStudies
                dateLabel.text = viewModel.getStartDate + " - " + viewModel.getFinishDate
            }
        }
    }
    
    let bgImage: UIImageView = {
        let bgImage = UIImageView(frame: CGRect.zero)
        bgImage.image = UIImage(named: "upiicsaIcon")
        bgImage.contentMode = UIView.ContentMode.scaleToFill
        bgImage.translatesAutoresizingMaskIntoConstraints = false
        return bgImage
    }()
    
    let schoolLabel: UILabel = {
        let schoolLabel = UILabel(frame: CGRect.zero)
        schoolLabel.text = "IPN UPIICSA"
        schoolLabel.font = UIFont(name: "DINCondensed-Bold", size: 16)
        schoolLabel.adjustsFontSizeToFitWidth = true
        schoolLabel.textColor = UIColor.customBlue
        schoolLabel.translatesAutoresizingMaskIntoConstraints = false
        return schoolLabel
    }()
    
    let studyLabel: UILabel = {
        let studyLabel = UILabel(frame: CGRect.zero)
        studyLabel.text = "Curso de desarrollo iOS nativo Intermedio"
        studyLabel.font = UIFont(name: "DINCondensed-Bold", size: 14)
        studyLabel.adjustsFontSizeToFitWidth = true
        studyLabel.textColor = UIColor.black
        studyLabel.translatesAutoresizingMaskIntoConstraints = false
        return studyLabel
    }()
    
    let dateLabel: UILabel = {
        let dateLabel = UILabel(frame: CGRect.zero)
        dateLabel.text = "2011-2016"
        dateLabel.font = UIFont(name: "DINCondensed-Bold", size: 12)
        dateLabel.adjustsFontSizeToFitWidth = true
        dateLabel.textColor = UIColor.gray
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        return dateLabel
    }()
    
    let stackView: UIStackView = {
        let stackView = UIStackView(frame: CGRect.zero)
        stackView.axis = NSLayoutConstraint.Axis.vertical
        stackView.distribution = UIStackView.Distribution.fillProportionally
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setSubviews()
        setAutolayout()
    }
    
    // Método para agregar los componentes a la celda
    private func setSubviews() {
        
        self.addSubview(bgImage)
        stackView.addArrangedSubview(schoolLabel)
        stackView.addArrangedSubview(studyLabel)
        stackView.addArrangedSubview(dateLabel)
        self.addSubview(stackView)
    }
    
    // Método para definir el autolayout de los componentes de la celda
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            bgImage.topAnchor.constraint(equalTo: self.topAnchor),
            bgImage.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            bgImage.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            bgImage.leadingAnchor.constraint(equalTo: self.leadingAnchor)
            ])
        
        NSLayoutConstraint.activate([
            stackView.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.3),
            stackView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10),
            stackView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10),
            stackView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10)
            ])
    }
}
