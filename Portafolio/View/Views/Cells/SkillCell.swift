//
//  SkillCell.swift
//  Portafolio
//
//  Created by David Valencia on 9/11/19.
//  Copyright © 2019 David. All rights reserved.
//

import UIKit

class SkillCell: UITableViewCell {
    
    var modelView: SkillsViewModel? {
        didSet {
            if let viewModel = modelView {
                skillLabel.text = viewModel.getSkill
            }
        }
    }
    
    let skillLabel: UILabel = {
        let skillLabel = UILabel(frame: CGRect.zero)
        skillLabel.text = ""
        skillLabel.font = UIFont(name: "DINCondensed-Bold", size: 16)
        skillLabel.adjustsFontSizeToFitWidth = true
        skillLabel.textColor = UIColor.customBlue
        skillLabel.translatesAutoresizingMaskIntoConstraints = false
        return skillLabel
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: "skillCell")
        
        setSubviews()
        setAutolayout()
    }
    
    private func setSubviews() {
        
        self.addSubview(skillLabel)
    }
    
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            skillLabel.topAnchor.constraint(equalTo: self.topAnchor),
            skillLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            skillLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20),
            skillLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20)
            ])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
