//
//  ProjectCell.swift
//  Portafolio
//
//  Created by David Valencia on 9/11/19.
//  Copyright © 2019 David. All rights reserved.
//

import UIKit

class ProjectCell: UICollectionViewCell {
    
    var modelView: ProjectViewModel? {
        didSet {
            if let viewModel = modelView {
                icon.image = UIImage(named: viewModel.getImageName + "Icon")
            }
        }
    }
    
    let icon: UIImageView = {
        let icon = UIImageView(frame: CGRect.zero)
        icon.image = UIImage(named: "protegmIcon")
        icon.contentMode = UIView.ContentMode.scaleToFill
        icon.layer.masksToBounds = true
        icon.layer.cornerRadius = 5.0
        icon.translatesAutoresizingMaskIntoConstraints = false
        return icon
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setSubviews()
        setAutolayout()
    }
    
    // Método para agregar los componentes a la celda
    private func setSubviews() {
        
        self.addSubview(icon)
    }
    
    // Método para definir el autolayout de los componentes de la vista
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            icon.topAnchor.constraint(equalTo: self.topAnchor),
            icon.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            icon.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            icon.leadingAnchor.constraint(equalTo: self.leadingAnchor)
            ])
    }
}
