//
//  ProjectsView.swift
//  Portafolio
//
//  Created by David Valencia on 9/11/19.
//  Copyright © 2019 David. All rights reserved.
//

import UIKit

class ProjectsView: UIView {
    
    var modelView: ProjectViewModel? {
        didSet {
            if let viewModel = modelView {
                projectLabel.text = viewModel.getName
                projectDescription.text = viewModel.getDescription
                linkLabel.text = viewModel.getLink
            }
        }
    }
    
    let bgImage: UIImageView = {
        let bgImage = UIImageView(frame: CGRect.zero)
        bgImage.image = UIImage(named: "bg3")
        bgImage.contentMode = UIView.ContentMode.scaleToFill
        bgImage.translatesAutoresizingMaskIntoConstraints = false
        return bgImage
    }()
    
    let overlay: UIView = {
        let overlay = UIView(frame: CGRect.zero)
        overlay.backgroundColor = UIColor.customWhite.withAlphaComponent(0.5)
        overlay.translatesAutoresizingMaskIntoConstraints = false
        return overlay
    }()
    
    let selectLabel: UILabel = {
        let selectLabel = UILabel(frame: CGRect.zero)
        selectLabel.numberOfLines = 0
        selectLabel.text = "Selecciona un proyecto para ver más detalle"
        selectLabel.font = UIFont(name: "DINAlternate-Bold", size: 16)
        selectLabel.textColor = UIColor.customBlue
        selectLabel.backgroundColor = UIColor.clear
        selectLabel.translatesAutoresizingMaskIntoConstraints = false
        return selectLabel
    }()
    
    let projectCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = UICollectionView.ScrollDirection.vertical
        
        let projectCollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        projectCollectionView.register(ProjectCell.self, forCellWithReuseIdentifier: "project")
        projectCollectionView.backgroundColor = UIColor.clear
        projectCollectionView.translatesAutoresizingMaskIntoConstraints = false
        projectCollectionView.allowsMultipleSelection = false
        return projectCollectionView
    }()
    
    let projectDescription: UILabel = {
        let projectDescription = UILabel(frame: CGRect.zero)
        projectDescription.text = ""
        projectDescription.numberOfLines = 0
        projectDescription.font = UIFont(name: "Helvetica", size: 15)
        projectDescription.textColor = UIColor.customBlue
        projectDescription.backgroundColor = UIColor.white.withAlphaComponent(0.6)
        projectDescription.textAlignment = NSTextAlignment.center
        projectDescription.translatesAutoresizingMaskIntoConstraints = false
        return projectDescription
    }()
    
    let projectLabel: UILabel = {
        let projectLabel = UILabel(frame: CGRect.zero)
        projectLabel.text = "PROTEGM"
        projectLabel.numberOfLines = 0
        projectLabel.font = UIFont(name: "DINCondensed-Bold", size: 30)
        projectLabel.backgroundColor = UIColor.customBlue
        projectLabel.textColor = UIColor.customWhite
        projectLabel.textAlignment = NSTextAlignment.center
        projectLabel.translatesAutoresizingMaskIntoConstraints = false
        return projectLabel
    }()
    
    let linkLabel: UITextView = {
        let linkLabel = UITextView(frame: CGRect.zero)
        linkLabel.text = ""
        linkLabel.font = UIFont(name: "Helvetica", size: 15)
        linkLabel.textColor = UIColor.customBlue
        linkLabel.backgroundColor = UIColor.white.withAlphaComponent(0.6)
        linkLabel.textAlignment = NSTextAlignment.center
        linkLabel.dataDetectorTypes = UIDataDetectorTypes.link
//        linkLabel.textContainerInset = UIEdgeInsets(top: 50, left: 0, bottom: 70, right: 0)
        linkLabel.isUserInteractionEnabled = true
        linkLabel.isEditable = false
        linkLabel.translatesAutoresizingMaskIntoConstraints = false
        return linkLabel
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setSubviews()
        setAutolayout()
    }
    
    // Método para agregar los componentes a la vista
    private func setSubviews() {
        
        self.addSubview(bgImage)
        self.addSubview(overlay)
        self.addSubview(selectLabel)
        self.addSubview(projectCollectionView)
        self.addSubview(projectDescription)
        self.addSubview(projectLabel)
        self.addSubview(linkLabel)
    }
    
    // Método para definir el autolayout de los componentes de la vista
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            bgImage.topAnchor.constraint(equalTo: self.topAnchor),
            bgImage.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            bgImage.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            bgImage.leadingAnchor.constraint(equalTo: self.leadingAnchor)
            ])
        
        NSLayoutConstraint.activate([
            overlay.topAnchor.constraint(equalTo: self.topAnchor),
            overlay.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            overlay.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            overlay.leadingAnchor.constraint(equalTo: self.leadingAnchor)
            ])
        
        NSLayoutConstraint.activate([
            selectLabel.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 20),
            selectLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20),
            selectLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20)
            ])
        
        NSLayoutConstraint.activate([
            projectCollectionView.topAnchor.constraint(equalTo: selectLabel.bottomAnchor, constant: 20),
            projectCollectionView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 15),
            projectCollectionView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.15),
            projectCollectionView.heightAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.9)
            ])
        
        NSLayoutConstraint.activate([
            projectLabel.centerYAnchor.constraint(equalTo: projectCollectionView.centerYAnchor, constant: 0),
            projectLabel.leadingAnchor.constraint(equalTo: projectCollectionView.trailingAnchor, constant: 25),
            projectLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -15),
            projectLabel.heightAnchor.constraint(equalToConstant: 50)
            ])
        
        NSLayoutConstraint.activate([
            projectDescription.topAnchor.constraint(equalTo: projectLabel.bottomAnchor, constant: 0),
            projectDescription.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0),
            projectDescription.leadingAnchor.constraint(equalTo: projectCollectionView.trailingAnchor, constant: 40)
            ])
        
        NSLayoutConstraint.activate([
            linkLabel.topAnchor.constraint(equalTo: projectDescription.bottomAnchor, constant: 0),
            linkLabel.leadingAnchor.constraint(equalTo: projectCollectionView.trailingAnchor, constant: 40),
            linkLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0),
            linkLabel.heightAnchor.constraint(equalToConstant: 50)
            ])
    }
}

