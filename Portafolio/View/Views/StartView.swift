//
//  StartView.swift
//  Portafolio
//
//  Created by David Valencia on 9/11/19.
//  Copyright © 2019 David. All rights reserved.
//

import UIKit

class StartView: UIView {
    
    let bgImage: UIImageView = {
        let bgImage = UIImageView(frame: CGRect.zero)
        bgImage.image = UIImage(named: "startBG")
        bgImage.contentMode = UIView.ContentMode.scaleToFill
        bgImage.translatesAutoresizingMaskIntoConstraints = false
        return bgImage
    }()
    
    let titleLabel: UILabel = {
        let titleLabel = UILabel(frame: CGRect.zero)
        titleLabel.text = "iOS Developer"
        titleLabel.font = UIFont(name: "DINCondensed-Bold", size: 35)
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.textColor = UIColor.customBlue
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        return titleLabel
    }()
    
    let nameTextView: UITextView = {
        let nameTextView = UITextView(frame: CGRect.zero)
        nameTextView.text = "César David Valencia Estrada"
        nameTextView.textColor = UIColor.customWhite
        nameTextView.font = UIFont(name: "Helvetica", size: 25)
        nameTextView.backgroundColor = UIColor.clear
        nameTextView.adjustsFontForContentSizeCategory = true
        nameTextView.isUserInteractionEnabled = false
        nameTextView.translatesAutoresizingMaskIntoConstraints = false
        return nameTextView
    }()
    
    let cvBtn: UIButton = {
        let cvBtn = UIButton(type: UIButton.ButtonType.custom)
        cvBtn.setAttributedTitle(NSAttributedString(string: "Ver Curriculum", attributes: [NSAttributedString.Key.font : UIFont(name: "Helvetica-Bold", size: 15) ?? UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.bold), NSAttributedString.Key.foregroundColor : UIColor.customBlue]), for: UIControl.State.normal)
        cvBtn.backgroundColor = UIColor.customWhite
        cvBtn.layer.masksToBounds = true
        cvBtn.layer.cornerRadius = 5.0
        cvBtn.translatesAutoresizingMaskIntoConstraints = false
        return cvBtn
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setSubviews()
        setAutolayout()
    }
    
    // Método para agregar los componentes a la vista
    private func setSubviews() {
        
        self.addSubview(bgImage)
        self.addSubview(titleLabel)
        self.addSubview(cvBtn)
        self.addSubview(nameTextView)
    }
    
    // Método para definir el autolayout de los componentes de la vista
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            bgImage.topAnchor.constraint(equalTo: self.topAnchor),
            bgImage.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            bgImage.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            bgImage.leadingAnchor.constraint(equalTo: self.leadingAnchor)
            ])
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 10),
            titleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20),
            titleLabel.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.5),
            titleLabel.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.1)
            ])
        
        NSLayoutConstraint.activate([
            cvBtn.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -40),
            cvBtn.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            cvBtn.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.85),
            cvBtn.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.075)
            ])
        
        NSLayoutConstraint.activate([
            nameTextView.bottomAnchor.constraint(equalTo: cvBtn.topAnchor, constant: -50),
            nameTextView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20),
            nameTextView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.65),
            nameTextView.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.12)
            ])
    }
}
