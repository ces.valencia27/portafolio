//
//  WorkExperienceView.swift
//  Portafolio
//
//  Created by David Valencia on 9/11/19.
//  Copyright © 2019 David. All rights reserved.
//

import UIKit
import MapKit

class WorkExperienceView: UIView {
    
    let bgImage: UIImageView = {
        let bgImage = UIImageView(frame: CGRect.zero)
        bgImage.image = UIImage(named: "bg3")
        bgImage.contentMode = UIView.ContentMode.scaleToFill
        bgImage.translatesAutoresizingMaskIntoConstraints = false
        return bgImage
    }()
    
    let overlay: UIView = {
        let overlay = UIView(frame: CGRect.zero)
        overlay.backgroundColor = UIColor.customWhite.withAlphaComponent(0.5)
        overlay.translatesAutoresizingMaskIntoConstraints = false
        return overlay
    }()
    
    let map: MKMapView = {
        let map = MKMapView(frame: CGRect.zero)
        map.isZoomEnabled = true
        map.isRotateEnabled = false
        map.showsUserLocation = true
        map.translatesAutoresizingMaskIntoConstraints = false
        return map
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setSubviews()
        setAutolayout()
    }
    
    // Método para agregar los componentes a la vista
    private func setSubviews() {
        
        self.addSubview(bgImage)
        self.addSubview(overlay)
        self.addSubview(map)
    }
    
    // Método para definir el autolayout de los componentes de la vista
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            bgImage.topAnchor.constraint(equalTo: self.topAnchor),
            bgImage.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            bgImage.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            bgImage.leadingAnchor.constraint(equalTo: self.leadingAnchor)
            ])
        
        NSLayoutConstraint.activate([
            overlay.topAnchor.constraint(equalTo: self.topAnchor),
            overlay.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            overlay.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            overlay.leadingAnchor.constraint(equalTo: self.leadingAnchor)
            ])
        
        NSLayoutConstraint.activate([
            map.topAnchor.constraint(equalTo: self.topAnchor, constant: 0),
            map.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0),
            map.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0),
            map.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0)
            ])
    }
}

