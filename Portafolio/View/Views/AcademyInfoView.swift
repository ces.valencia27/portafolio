//
//  AcademyInfoView.swift
//  Portafolio
//
//  Created by David Valencia on 9/11/19.
//  Copyright © 2019 David. All rights reserved.
//

import UIKit

class AcademyInfoView: UIView {
    
    let bgImage: UIImageView = {
        let bgImage = UIImageView(frame: CGRect.zero)
        bgImage.image = UIImage(named: "bg3")
        bgImage.contentMode = UIView.ContentMode.scaleToFill
        bgImage.translatesAutoresizingMaskIntoConstraints = false
        return bgImage
    }()
    
    let overlay: UIView = {
        let overlay = UIView(frame: CGRect.zero)
        overlay.backgroundColor = UIColor.customWhite.withAlphaComponent(0.5)
        overlay.translatesAutoresizingMaskIntoConstraints = false
        return overlay
    }()
    
    let academyCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
        layout.minimumLineSpacing = 20
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = UICollectionView.ScrollDirection.horizontal
        
        let academyCollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        academyCollectionView.register(SchoolCell.self, forCellWithReuseIdentifier: "school")
        academyCollectionView.backgroundColor = UIColor.clear
        academyCollectionView.translatesAutoresizingMaskIntoConstraints = false
        academyCollectionView.allowsSelection = false
        return academyCollectionView
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setSubviews()
        setAutolayout()
    }
    
    // Método para agregar los compnentes a la vista
    private func setSubviews() {
        
        self.addSubview(bgImage)
        self.addSubview(overlay)
        self.addSubview(academyCollectionView)
    }
    
    // Método para definir el autolayout de los componentes de la vista
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            bgImage.topAnchor.constraint(equalTo: self.topAnchor),
            bgImage.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            bgImage.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            bgImage.leadingAnchor.constraint(equalTo: self.leadingAnchor)
            ])
        
        NSLayoutConstraint.activate([
            overlay.topAnchor.constraint(equalTo: self.topAnchor),
            overlay.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            overlay.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            overlay.leadingAnchor.constraint(equalTo: self.leadingAnchor)
            ])
        
        NSLayoutConstraint.activate([
            academyCollectionView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            academyCollectionView.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            academyCollectionView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1),
            academyCollectionView.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.3)
            ])
    }
}

