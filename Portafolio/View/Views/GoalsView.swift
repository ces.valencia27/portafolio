//
//  GoalsView.swift
//  Portafolio
//
//  Created by David Valencia on 9/11/19.
//  Copyright © 2019 David. All rights reserved.
//

import UIKit

class GoalsView: UIView {
    
    var modelView: GoalsViewModel? {
        didSet {
            if let viewModel = modelView {
                goalsDescription.text = viewModel.getGoals
            } else {
                goalsDescription.text = "Description"
            }
        }
    }
    
    let blurBG: UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.regular)
        
        let blurBG = UIVisualEffectView(effect: blurEffect)
        blurBG.frame = CGRect.zero
        blurBG.translatesAutoresizingMaskIntoConstraints = false
        return blurBG
    }()
    
    let goalsDescription: UILabel = {
        let goalsDescription = UILabel(frame: CGRect.zero)
        goalsDescription.text = ""
        goalsDescription.numberOfLines = 0
        goalsDescription.font = UIFont(name: "AvenirNext-Medium", size: 14)
        goalsDescription.textColor = UIColor.black
        goalsDescription.backgroundColor = UIColor.white.withAlphaComponent(0)
        goalsDescription.layer.masksToBounds = true
        goalsDescription.layer.cornerRadius = 5.0
        goalsDescription.textAlignment = NSTextAlignment.justified
        goalsDescription.translatesAutoresizingMaskIntoConstraints = false
        return goalsDescription
    }()
    
    let image: UIImageView = {
        let image = UIImageView(frame: CGRect.zero)
        image.image = UIImage(named: "goalsImage")
        image.contentMode = UIView.ContentMode.scaleToFill
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setSubviews()
        setAutolayout()
    }
    
    // Método para agregar los componentes a la vista
    private func setSubviews() {
        
        self.addSubview(blurBG)
        self.addSubview(image)
        self.addSubview(goalsDescription)
    }
    
    // Método para definir el autolayout de los componentes de la vista
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            blurBG.topAnchor.constraint(equalTo: self.topAnchor, constant: 0),
            blurBG.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0),
            blurBG.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0),
            blurBG.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0)
            ])
        
        NSLayoutConstraint.activate([
            image.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20),
            image.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 50),
            image.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20),
            image.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.3)
            ])
        
        NSLayoutConstraint.activate([
            goalsDescription.topAnchor.constraint(equalTo: image.bottomAnchor, constant: 50),
            goalsDescription.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20),
            goalsDescription.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20)
            ])
    }
}

