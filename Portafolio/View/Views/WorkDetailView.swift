//
//  WorkDetailView.swift
//  Portafolio
//
//  Created by David Valencia on 9/11/19.
//  Copyright © 2019 David. All rights reserved.
//

import UIKit

class WorkDetailView: UIView {
    
    var modelView: JobPlaceViewModel? {
        didSet {
            if let viewModel = modelView {
                let text1: NSAttributedString = NSAttributedString(string: "\(viewModel.getName) ", attributes: [NSAttributedString.Key.font : UIFont(name: "DINCondensed-Bold", size: 19)!, NSAttributedString.Key.foregroundColor : UIColor.customBlue])
                
                let text2: NSAttributedString = NSAttributedString(string: "\(viewModel.getDate)\n", attributes: [NSAttributedString.Key.font : UIFont(name: "DINAlternate-Bold", size: 13)!, NSAttributedString.Key.foregroundColor : UIColor.customBlue])
                
                let fullTextJobName = NSMutableAttributedString()
                
                fullTextJobName.append(text1)
                fullTextJobName.append(text2)
                
                jobNameLabel.attributedText = fullTextJobName
                
                let text3: NSAttributedString = NSAttributedString(string: "Puesto: ", attributes: [NSAttributedString.Key.font : UIFont(name: "DINCondensed-Bold", size: 19)!, NSAttributedString.Key.foregroundColor : UIColor.customBlue])
                
                let text4: NSAttributedString = NSAttributedString(string: "\(viewModel.getPosition)\n", attributes: [NSAttributedString.Key.font : UIFont(name: "DINAlternate-Bold", size: 16)!, NSAttributedString.Key.foregroundColor : UIColor.customBlue])
                
                let fullTextPosition = NSMutableAttributedString()
                fullTextPosition.append(text3)
                fullTextPosition.append(text4)
                
                positionLabel.attributedText = fullTextPosition
                
                let text5: NSAttributedString = NSAttributedString(string: "Actividades: ", attributes: [NSAttributedString.Key.font : UIFont(name: "DINCondensed-Bold", size: 19)!, NSAttributedString.Key.foregroundColor : UIColor.customBlue])
                
                let text6: NSAttributedString = NSAttributedString(string: "\(viewModel.getDetail)\n\n", attributes: [NSAttributedString.Key.font : UIFont(name: "DINAlternate-Bold", size: 16)!, NSAttributedString.Key.foregroundColor : UIColor.customBlue])
                
                let fullTextActivities = NSMutableAttributedString()
                fullTextActivities.append(text5)
                fullTextActivities.append(text6)
                
                activitiesLabel.attributedText = fullTextActivities
            }
        }
    }
    
    let bgImage: UIImageView = {
        let bgImage = UIImageView(frame: CGRect.zero)
        bgImage.image = UIImage(named: "bg3")
        bgImage.contentMode = UIView.ContentMode.scaleToFill
        bgImage.translatesAutoresizingMaskIntoConstraints = false
        return bgImage
    }()
    
    let overlay: UIView = {
        let overlay = UIView(frame: CGRect.zero)
        overlay.backgroundColor = UIColor.customWhite.withAlphaComponent(0.8)
        overlay.translatesAutoresizingMaskIntoConstraints = false
        return overlay
    }()
    
    let jobNameLabel: UILabel = {
        let jobNameLabel = UILabel(frame: CGRect.zero)
        jobNameLabel.numberOfLines = 0
        jobNameLabel.attributedText = NSAttributedString()
        jobNameLabel.textColor = UIColor.customBlue
        jobNameLabel.backgroundColor = UIColor.clear
        jobNameLabel.translatesAutoresizingMaskIntoConstraints = false
        return jobNameLabel
    }()
    
    let positionLabel: UILabel = {
        let positionLabel = UILabel(frame: CGRect.zero)
        positionLabel.numberOfLines = 0
        positionLabel.attributedText = NSAttributedString()
        positionLabel.textColor = UIColor.customBlue
        positionLabel.backgroundColor = UIColor.clear
        positionLabel.translatesAutoresizingMaskIntoConstraints = false
        return positionLabel
    }()
    
    let activitiesLabel: UILabel = {
        let activitiesLabel = UILabel(frame: CGRect.zero)
        activitiesLabel.numberOfLines = 0
        activitiesLabel.attributedText = NSAttributedString()
        activitiesLabel.textColor = UIColor.customBlue
        activitiesLabel.backgroundColor = UIColor.clear
        activitiesLabel.textAlignment = NSTextAlignment.justified
        activitiesLabel.translatesAutoresizingMaskIntoConstraints = false
        return activitiesLabel
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setSubviews()
        setAutolayout()
    }
    
    // Método para agregar los componentes a la vista
    private func setSubviews() {
        
        self.addSubview(bgImage)
        self.addSubview(overlay)
        self.addSubview(jobNameLabel)
        self.addSubview(positionLabel)
        self.addSubview(activitiesLabel)
    }
    
    // Método para definir el autolayout de los componentes de la vista
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            bgImage.topAnchor.constraint(equalTo: self.topAnchor),
            bgImage.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            bgImage.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            bgImage.leadingAnchor.constraint(equalTo: self.leadingAnchor)
            ])
        
        NSLayoutConstraint.activate([
            overlay.topAnchor.constraint(equalTo: self.topAnchor),
            overlay.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            overlay.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            overlay.leadingAnchor.constraint(equalTo: self.leadingAnchor)
            ])
        
        NSLayoutConstraint.activate([
            jobNameLabel.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 30),
            jobNameLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20),
            jobNameLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20)
            ])
        
        NSLayoutConstraint.activate([
            positionLabel.topAnchor.constraint(equalTo: jobNameLabel.bottomAnchor, constant: 10),
            positionLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20),
            positionLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20)
            ])
        
        NSLayoutConstraint.activate([
            activitiesLabel.topAnchor.constraint(equalTo: positionLabel.bottomAnchor, constant: 10),
            activitiesLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20),
            activitiesLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20)
            ])
    }
    
}

