//
//  PersonalInfoView.swift
//  Portafolio
//
//  Created by David Valencia on 9/11/19.
//  Copyright © 2019 David. All rights reserved.
//

import UIKit

class PersonalInfoView: UIView {
    
    var modelView: PersonalInfoViewModel? {
        didSet {
            if let viewModel = modelView {
                nameLabel.text = viewModel.getName
                positionLabel.text = viewModel.getPosition
                mailLabel.text = viewModel.getMail
                phoneLabel.text = viewModel.getPhone
            }
        }
    }
    
    let bgImage: UIImageView = {
        let bgImage = UIImageView(frame: CGRect.zero)
        bgImage.image = UIImage(named: "bg3")
        bgImage.contentMode = UIView.ContentMode.scaleToFill
        bgImage.translatesAutoresizingMaskIntoConstraints = false
        return bgImage
    }()
    
    let overlay: UIView = {
        let overlay = UIView(frame: CGRect.zero)
        overlay.backgroundColor = UIColor.customWhite.withAlphaComponent(0.5)
        overlay.translatesAutoresizingMaskIntoConstraints = false
        return overlay
    }()
    
    let infoContainer: UIView = {
        let infoContainer = UIView(frame: CGRect.zero)
        infoContainer.backgroundColor = UIColor.customBlue.withAlphaComponent(0.8)
        infoContainer.layer.masksToBounds = true
        infoContainer.layer.cornerRadius = 10.0
        infoContainer.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        infoContainer.layer.shadowRadius = 5.0
        infoContainer.layer.shadowColor = UIColor.black.cgColor
        infoContainer.layer.shadowOpacity = 0.8
        infoContainer.layer.borderWidth = 0.5
        infoContainer.layer.borderColor = UIColor.black.withAlphaComponent(0.2).cgColor
        infoContainer.translatesAutoresizingMaskIntoConstraints = false
        return infoContainer
    }()
    
    let blurBG: UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.regular)
        
        let blurBG = UIVisualEffectView(effect: blurEffect)
        blurBG.frame = CGRect.zero
        blurBG.layer.masksToBounds = true
        blurBG.layer.cornerRadius = 10.0
        blurBG.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        blurBG.layer.shadowRadius = 5.0
        blurBG.layer.maskedCorners = [CACornerMask.layerMinXMinYCorner, CACornerMask.layerMinXMaxYCorner]
        blurBG.layer.shadowColor = UIColor.black.cgColor
        blurBG.layer.shadowOpacity = 0.8
        blurBG.layer.borderWidth = 0.5
        blurBG.layer.borderColor = UIColor.customBlue.withAlphaComponent(0.2).cgColor
        blurBG.backgroundColor = UIColor.customBlue.withAlphaComponent(0.7)
        blurBG.translatesAutoresizingMaskIntoConstraints = false
        return blurBG
    }()
    
    let profileImage: UIImageView = {
        let profileImage = UIImageView(frame: CGRect.zero)
        profileImage.image = UIImage(named: "profileImage")
        profileImage.contentMode = UIView.ContentMode.scaleToFill
        profileImage.layer.masksToBounds = true
        profileImage.layer.cornerRadius = 8.0
        profileImage.translatesAutoresizingMaskIntoConstraints = false
        return profileImage
    }()
    
    let nameLabel: UILabel = {
        let nameLabel = UILabel(frame: CGRect.zero)
        nameLabel.text = ""
        nameLabel.font = UIFont(name: "DINAlternate-Bold", size: 13)
        nameLabel.adjustsFontSizeToFitWidth = true
        nameLabel.textColor = UIColor.customWhite
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        return nameLabel
    }()
    
    let positionLabel: UILabel = {
        let positionLabel = UILabel(frame: CGRect.zero)
        positionLabel.text = ""
        positionLabel.font = UIFont(name: "DINAlternate-Bold", size: 13)
        positionLabel.adjustsFontSizeToFitWidth = true
        positionLabel.textColor = UIColor.customWhite
        positionLabel.translatesAutoresizingMaskIntoConstraints = false
        return positionLabel
    }()
    
    let mailLabel: UILabel = {
        let mailLabel = UILabel(frame: CGRect.zero)
        mailLabel.text = ""
        mailLabel.font = UIFont(name: "DINAlternate-Bold", size: 13)
        mailLabel.adjustsFontSizeToFitWidth = true
        mailLabel.textColor = UIColor.customWhite
        mailLabel.translatesAutoresizingMaskIntoConstraints = false
        return mailLabel
    }()
    
    let phoneLabel: UILabel = {
        let phoneLabel = UILabel(frame: CGRect.zero)
        phoneLabel.text = ""
        phoneLabel.font = UIFont(name: "DINAlternate-Bold", size: 13)
        phoneLabel.adjustsFontSizeToFitWidth = true
        phoneLabel.textColor = UIColor.customWhite
        phoneLabel.translatesAutoresizingMaskIntoConstraints = false
        return phoneLabel
    }()
    
    let stackView: UIStackView = {
        let stackView = UIStackView(frame: CGRect.zero)
        stackView.axis = NSLayoutConstraint.Axis.vertical
        stackView.distribution = UIStackView.Distribution.fillEqually
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    let skillsLabel: UILabel = {
        let skillsLabel = UILabel(frame: CGRect.zero)
        skillsLabel.text = "HABILIDADES"
        skillsLabel.font = UIFont(name: "DINCondensed-Bold", size: 20)
        skillsLabel.adjustsFontSizeToFitWidth = true
        skillsLabel.textColor = UIColor.customWhite
        skillsLabel.textAlignment = NSTextAlignment.center
        skillsLabel.translatesAutoresizingMaskIntoConstraints = false
        return skillsLabel
    }()
    
    let segmented: UISegmentedControl = {
        let segmented = UISegmentedControl(items: ["Profesionales", "Personales"])
        segmented.tintColor = UIColor.customWhite
        segmented.selectedSegmentIndex = 0
        segmented.translatesAutoresizingMaskIntoConstraints = false
        return segmented
    }()
    
    let skillsTable: UITableView = {
        let skillsTable = UITableView(frame: CGRect.zero)
        skillsTable.register(SkillCell.self, forCellReuseIdentifier: "skillCell")
        skillsTable.backgroundColor = UIColor.customWhite
        skillsTable.layer.masksToBounds = true
        skillsTable.layer.cornerRadius = 10.0
        skillsTable.layer.maskedCorners = [CACornerMask.layerMinXMaxYCorner, CACornerMask.layerMaxXMaxYCorner]
        skillsTable.bounces = false
        skillsTable.translatesAutoresizingMaskIntoConstraints = false
        return skillsTable
    }()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setSubviews()
        setAutolayout()
    }
    
    // Método para agregar los componentes a la vista
    private func setSubviews() {
        
        self.addSubview(bgImage)
        self.addSubview(overlay)
        self.addSubview(infoContainer)
        //        self.addSubview(blurBG)
        self.addSubview(profileImage)
        stackView.addArrangedSubview(nameLabel)
        stackView.addArrangedSubview(positionLabel)
        stackView.addArrangedSubview(mailLabel)
        stackView.addArrangedSubview(phoneLabel)
        infoContainer.addSubview(stackView)
        infoContainer.addSubview(skillsLabel)
        infoContainer.addSubview(segmented)
        infoContainer.addSubview(skillsTable)
    }
    
    // Método para definir el autolayout de los componentes de la vista
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            bgImage.topAnchor.constraint(equalTo: self.topAnchor),
            bgImage.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            bgImage.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            bgImage.leadingAnchor.constraint(equalTo: self.leadingAnchor)
            ])
        
        NSLayoutConstraint.activate([
            overlay.topAnchor.constraint(equalTo: self.topAnchor),
            overlay.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            overlay.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            overlay.leadingAnchor.constraint(equalTo: self.leadingAnchor)
            ])
        
        NSLayoutConstraint.activate([
            infoContainer.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 30),
            infoContainer.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -15),
            infoContainer.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -30),
            infoContainer.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 15)
            ])
        
        //        NSLayoutConstraint.activate([
        //            blurBG.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 30),
        //            blurBG.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0),
        //            blurBG.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: -30),
        //            blurBG.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.8)
        //            ])
        
        NSLayoutConstraint.activate([
            profileImage.topAnchor.constraint(equalTo: infoContainer.topAnchor, constant: 20),
            profileImage.leadingAnchor.constraint(equalTo: infoContainer.leadingAnchor, constant: 20),
            profileImage.widthAnchor.constraint(equalTo: infoContainer.widthAnchor, multiplier: 0.25),
            profileImage.heightAnchor.constraint(equalTo: infoContainer.widthAnchor, multiplier: 0.25)
            ])
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: infoContainer.topAnchor, constant: 20),
            stackView.leadingAnchor.constraint(equalTo: profileImage.trailingAnchor, constant: 10),
            stackView.trailingAnchor.constraint(equalTo: infoContainer.trailingAnchor, constant: -20),
            stackView.heightAnchor.constraint(equalTo: infoContainer.widthAnchor, multiplier: 0.25)
            ])
        
        NSLayoutConstraint.activate([
            skillsLabel.topAnchor.constraint(equalTo: stackView.bottomAnchor, constant: 20),
            skillsLabel.leadingAnchor.constraint(equalTo: infoContainer.leadingAnchor, constant: 20),
            skillsLabel.trailingAnchor.constraint(equalTo: infoContainer.trailingAnchor, constant: -20),
            skillsLabel.heightAnchor.constraint(equalTo: infoContainer.heightAnchor, multiplier: 0.07)
            ])
        
        NSLayoutConstraint.activate([
            segmented.topAnchor.constraint(equalTo: skillsLabel.bottomAnchor, constant: 0),
            segmented.leadingAnchor.constraint(equalTo: infoContainer.leadingAnchor, constant: 20),
            segmented.trailingAnchor.constraint(equalTo: infoContainer.trailingAnchor, constant: -20),
            segmented.heightAnchor.constraint(equalTo: infoContainer.heightAnchor, multiplier: 0.05)
            ])
        
        NSLayoutConstraint.activate([
            skillsTable.topAnchor.constraint(equalTo: segmented.bottomAnchor, constant: 20),
            skillsTable.trailingAnchor.constraint(equalTo: infoContainer.trailingAnchor, constant: 0),
            skillsTable.bottomAnchor.constraint(equalTo: infoContainer.bottomAnchor, constant: 0),
            skillsTable.leadingAnchor.constraint(equalTo: infoContainer.leadingAnchor, constant: 0)
            ])
    }
}

