//
//  WorkExperienceVC.swift
//  Portafolio
//
//  Created by David Valencia on 9/11/19.
//  Copyright © 2019 David. All rights reserved.
//

import UIKit
import MapKit

class WorkExperienceVC: UIViewController {
    
    let interface: WorkExperienceView = {
        let interface = WorkExperienceView(frame: CGRect.zero)
        interface.backgroundColor = UIColor.customWhite
        interface.translatesAutoresizingMaskIntoConstraints = false
        return interface
    }()
    
    let locationManager = CLLocationManager()
    let regionInMeters: Double = 2000000
    var jobModels = [JobPlaceModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Cargando la interfaz gráfica del controlador
        loadInterface()
        
    }
    
    // Método para cargar la interfaz gráfica del controlador
    private func loadInterface() {
        
        setSubviews()
        setAutolayout()
        checkLocationServices()
        setDelegates()
        validateConnection()
    }
    
    // Método para agregar la interfaz gráfica a la vista principal del controlador
    private func setSubviews() {
        
        self.view.addSubview(interface)
    }
    
    // Método para definir el autolayout de la interfaz gráfica
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            interface.topAnchor.constraint(equalTo: self.view.topAnchor),
            interface.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            interface.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            interface.leadingAnchor.constraint(equalTo: self.view.leadingAnchor)
            ])
    }
    
    //Método para asignar los objetos que actuarán como delegados
    private func setDelegates() {
        
        interface.map.delegate = self
    }
    
    // Método para hacer la petición y obtener la información de los trabajos anteriores
    private func request() {
        
        let networkManager = URL_Session()
        networkManager.delegate = self
        networkManager.getRequest(endpoint: Endpoints.shared().work_experience)
        LoaderView.shared().showActivityIndicatory(uiView: self.view)
    }
    
    // Método para validar si existe conexión a internet y hacer la petición o extraer la información del coreData
    private func validateConnection() {
        
        if NetworkConnection.isConnectedToNetwork() {
            request()
        } else {
            do {
                let jobData = try fetchJobData()
                jobModels.removeAll()
                jobModels = jobData
                interface.map.addAnnotations(jobModels)
            } catch let error {
                debugPrint(error.localizedDescription)
            }

            tabBarController?.present(AlertView.shared().showAlert(message: "Verifica tu conexión a internet"), animated: true, completion: nil)
        }
    }
    
    // MARK: - Métodos de ciclo de vida del controlador
    override func viewWillAppear(_ animated: Bool) {}
    
    override func viewWillDisappear(_ animated: Bool) {}
    
    override func viewDidAppear(_ animated: Bool) {}
    
    override func viewDidDisappear(_ animated: Bool) {}
    
    override func didReceiveMemoryWarning() {
        debugPrint("Problema de memoria")
    }
    
}
