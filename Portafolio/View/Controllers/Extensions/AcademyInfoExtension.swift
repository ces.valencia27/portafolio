//
//  AcademyInfoExtension.swift
//  Portafolio
//
//  Created by David Valencia on 9/11/19.
//  Copyright © 2019 David. All rights reserved.
//

import Foundation
import UIKit
import CoreData

extension AcademyInfoVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    // MARK: - Métodos de protocolo
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return schoolArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "school", for: indexPath) as! SchoolCell
        cell.backgroundColor = UIColor.white
        cell.layer.shadowOffset = CGSize(width: -0.2, height: 5.0)
        cell.layer.shadowRadius = 5.0
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOpacity = 0.5
        cell.layer.masksToBounds = true
        cell.layer.cornerRadius = 5.0
        
        let viewModel = SchoolViewModel(school: schoolArray[indexPath.item])
        
        cell.modelView = viewModel
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let wCell = interface.academyCollectionView.frame.width/2.5
        let hCell = interface.academyCollectionView.frame.height * 0.9
        
        return CGSize(width: wCell, height: hCell)
    }
    
}

extension AcademyInfoVC: URL_SessionDelegate {
    
    // Métodos de protocolo URL_SessionDelegate
    func connectionFinishSuccessfull(session: URL_Session, response: NSDictionary) {
        
        SchoolModel.getDataModel(dict: response) { (jsonArray, respuesta) in
            
            if respuesta == true {
                schoolArray.removeAll()
                schoolArray = jsonArray ?? []
                interface.academyCollectionView.reloadData()
                
                DBManager.shared.cleanEntity(currentEntity: SchoolEntity.self)
                
                do {
                    for item in jsonArray ?? [] {
                        try insertAcademyData(model: item)
                    }
                } catch let error {
                    debugPrint(error.localizedDescription)
                }
            } else {
               debugPrint("No hay registros")
            }
            
            LoaderView.shared().hideActivityIndicator(uiView: self.view)
        }
    }
    
    func connectionFinishWithError(session: URL_Session, error: Error) {
        
        LoaderView.shared().hideActivityIndicator(uiView: self.view)
        
        self.navigationController?.present(AlertView.shared().showAlert(message: error.localizedDescription), animated: true, completion: nil)
    }
    
}

extension AcademyInfoVC {
    
    var getContext: NSManagedObjectContext { return Bridge.context }
    
    func insertAcademyData(model: SchoolModel) throws {
        let entity = SchoolEntity(context: getContext)
        
        entity.schoolName = model.schoolName
        entity.estudies = model.estudies
        entity.fecha_inicio = model.fecha_inicio
        entity.fecha_final = model.fecha_final
        entity.image_name = model.image_name
        
        getContext.insert(entity)
        try getContext.save()
    }
    
    func fetchAcademyData() throws -> [SchoolModel] {
        var jsonArray = [SchoolModel]()
        let data = try getContext.fetch(SchoolEntity.fetchRequest() as NSFetchRequest<SchoolEntity>)
        
        for item in data {
            let model = SchoolModel(entity: item)
            jsonArray.append(model)
        }
        
        return jsonArray
    }
}
