//
//  PersonalInfoExtension.swift
//  Portafolio
//
//  Created by David Valencia on 9/11/19.
//  Copyright © 2019 David. All rights reserved.
//

import Foundation
import UIKit
import CoreData

extension PersonalInfoVC: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Métodos de protocolo UITableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return filterArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "skillCell", for: indexPath) as? SkillCell {

            let viewModel = SkillsViewModel(skillModel: filterArray[indexPath.row])
            cell.modelView = viewModel
            return cell
            
        } else {
            return UITableViewCell()
        }

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
}

extension PersonalInfoVC: URL_SessionDelegate {
    func connectionFinishSuccessfull(session: URL_Session, response: NSDictionary) {
        
        LoaderView.shared().hideActivityIndicator(uiView: self.tabBarController!.view)
        
        guard let service_name = response["service_name"] as? String else {
            fatalError("No fue posible obtener el valor")
        }
        
        if service_name == "personal_info" {
            if let object = PersonalInfoModel.getDataModel(dict: response) {
                
                let viewModel = PersonalInfoViewModel(info: object)
                interface.modelView = viewModel
                
                DBManager.shared.cleanEntity(currentEntity: PersonalInfoEntity.self)
                do {
                    try insertPersonalInfo(model: object)
                } catch let error {
                    print(error.localizedDescription)
                }
            } else {
                let alert = AlertView.shared().showAlert(message: "Ocurrió un error, espere unos segundos y vuelva a intentarlo")
                tabBarController?.present(alert, animated: true, completion: nil)
            }
        } else {
            SkillsModel.getPersonalSkills(dict: response) { (jsonArray, respuesta) in
                if respuesta {
                    skillsArray.removeAll()
                    guard let array = jsonArray else {
                        fatalError("Ocurrió un error al obtener el valor")
                    }
                    
                    DBManager.shared.cleanEntity(currentEntity: SkillEntity.self)
                    
                    do {
                        for item in array {
                            try insertSkills(model: item)
                        }
                    } catch let error {
                        debugPrint(error.localizedDescription)
                    }
                    
                    skillsArray = array
                    self.filterArray.removeAll()
                    self.filterArray = skillsArray.filter({$0.skillType == "profesional"})
                    self.interface.skillsTable.reloadData()
                } else {
                    let alert = AlertView.shared().showAlert(message: "Ocurrió un error, espere unos segundos y vuelva a intentarlo")
                    tabBarController?.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    func connectionFinishWithError(session: URL_Session, error: Error) {
        LoaderView.shared().hideActivityIndicator(uiView: self.tabBarController!.view)
        
        self.navigationController?.present(AlertView.shared().showAlert(message: error.localizedDescription), animated: true, completion: nil)
    }
    
}

extension PersonalInfoVC {
    
    var getContext: NSManagedObjectContext { return Bridge.context }
    
    func insertPersonalInfo(model: PersonalInfoModel) throws {
        let entity = PersonalInfoEntity(context: getContext)
        
        entity.name = model.name
        entity.position = model.position
        entity.mail = model.mail
        entity.phone_number = model.phone
        
        getContext.insert(entity)
        try getContext.save()
    }
    
    func fetchPersonalInfo() throws -> PersonalInfoModel {
        let data = try getContext.fetch(PersonalInfoEntity.fetchRequest() as NSFetchRequest<PersonalInfoEntity>)
        
        guard let personalInfo = data.first else {
            fatalError("No se pudo extraer el registro")
        }
        
        let model = PersonalInfoModel(entity: personalInfo)
        
        return model
    }
    
    func insertSkills(model: SkillsModel) throws {
        let entity = SkillEntity(context: getContext)
        
        entity.skill_name = model.skillName
        entity.skill_type = model.skillType
        
        getContext.insert(entity)
        try getContext.save()
    }
    
    func fetchSkills() throws -> [SkillsModel] {
        var jsonArray = [SkillsModel]()
        let data = try getContext.fetch(SkillEntity.fetchRequest() as NSFetchRequest<SkillEntity>)
        
        for item in data {
            let model = SkillsModel(entity: item)
            jsonArray.append(model)
        }
        
        return jsonArray
    }
    
}
