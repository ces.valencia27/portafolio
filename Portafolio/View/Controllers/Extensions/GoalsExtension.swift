//
//  GoalsExtension.swift
//  Portafolio
//
//  Created by David Valencia on 9/11/19.
//  Copyright © 2019 David. All rights reserved.
//

import Foundation
import UIKit
import CoreData

extension GoalsVC: URL_SessionDelegate {

    // Métodos de protocolo
    func connectionFinishSuccessfull(session: URL_Session, response: NSDictionary) {
        
        let dataModel = GoalsModel(dictionary: response)
        let viewModel = GoalsViewModel(goals: dataModel)
        interface.modelView = viewModel
        
        DBManager.shared.cleanEntity(currentEntity: GoalsEntity.self)
        
        do {
            try insertGoalsDta(model: dataModel)
        } catch let error {
            debugPrint(error.localizedDescription)
        }
        
        LoaderView.shared().hideActivityIndicator(uiView: self.view)
    }
    
    func connectionFinishWithError(session: URL_Session, error: Error) {
        LoaderView.shared().hideActivityIndicator(uiView: self.view)
        
        self.navigationController?.present(AlertView.shared().showAlert(message: error.localizedDescription), animated: true, completion: nil)
    }
}

extension GoalsVC {
    
    var getContext: NSManagedObjectContext { return Bridge.context }
    
    // MARK: - Método para insertar la información de los objetivos profesionales en core data
    func insertGoalsDta(model: GoalsModel) throws {
        let entity = GoalsEntity(context: getContext)
        
        entity.goals = model.goals
        
        getContext.insert(entity)
        try getContext.save()
    }
    
    // MARK: - Método para extraer la información de los objetivos profesionales de Core Data
    func fetchGoalsData() throws -> GoalsModel {
        let data = try getContext.fetch(GoalsEntity.fetchRequest() as NSFetchRequest<GoalsEntity>)
        
        guard let goalsData = data.first else {
            fatalError("No se pudo extraer el registro")
        }
        
        let model = GoalsModel(entity: goalsData)
        
        return model
    }
}
