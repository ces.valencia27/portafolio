//
//  WorkExperienceExtension.swift
//  Portafolio
//
//  Created by David Valencia on 9/11/19.
//  Copyright © 2019 David. All rights reserved.
//

import Foundation
import MapKit
import CoreData

extension WorkExperienceVC: URL_SessionDelegate {
    
    func connectionFinishSuccessfull(session: URL_Session, response: NSDictionary) {
        
        JobPlaceModel.getDataModel(dict: response) { (jsonArray, respuesta) in
            
            if respuesta == true {
                DBManager.shared.cleanEntity(currentEntity: JobEntity.self)
                
                do {
                    for item in jsonArray ?? [] {
                        try insertJobData(model: item)
                    }
                } catch let error {
                    debugPrint(error.localizedDescription)
                }
                
                jobModels = jsonArray ?? []
                interface.map.addAnnotations(jobModels)
            } else {
                debugPrint("No hay registros")
            }
            
            LoaderView.shared().hideActivityIndicator(uiView: self.view)
        }
    }
    
    func connectionFinishWithError(session: URL_Session, error: Error) {
        LoaderView.shared().hideActivityIndicator(uiView: self.view)
        
        self.navigationController?.present(AlertView.shared().showAlert(message: error.localizedDescription), animated: true, completion: nil)
    }
    
}

extension WorkExperienceVC: MKMapViewDelegate, CLLocationManagerDelegate {
    
    // MARK: - Métodos de protocolo
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkAuthorization()
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        guard let annotation = annotation as? JobPlaceModel else { return nil }
        
        let identifier = "marker"
        var view: MKMarkerAnnotationView
        
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            as? MKMarkerAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
        } else {
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5, y: 5)
            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        return view
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        
        if control == view.rightCalloutAccessoryView {
            if let jobMarker = view.annotation as? JobPlaceModel {
                let nextVC = WorkDetailVC()
                nextVC.currentJob = jobMarker
                navigationController?.pushViewController(nextVC, animated: true)
            }
        }
        
    }
    
    // Métodos para iniciar la configuración inicial, solicitud de permisos de localización, status de permisos...
    func setLocationManager() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
    }
    
    func centerView() {
        if let location = locationManager.location?.coordinate {
            let region = MKCoordinateRegion.init(center: location, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters)
            interface.map.setRegion(region, animated: true)
        }
    }
    
    // Método para validar que el servicio de ubicación esté habilitado
    func checkLocationServices() {
        if CLLocationManager.locationServicesEnabled() {
            setLocationManager()
            checkAuthorization()
        } else {
            let alert = AlertView.shared().showAlert(message: "Para proporcionarte la información, es necesario activar los permisos de localización en Configuración -> Proivacidad -> Localización")
            parent?.present(alert, animated: true, completion: nil)
        }
    }
    
    // Método para validar el status de autorización
    func checkAuthorization() {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            interface.map.showsUserLocation = true
            centerView()
            locationManager.startUpdatingLocation()
            break
        case .denied:
            let alert = UIAlertController(title: "Aviso", message: "Para poder visualizar la información es necesario proporcionar los permisos a la aplicación", preferredStyle: UIAlertController.Style.alert)
            let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
            alert.addAction(action)
            parent?.present(alert, animated: true, completion: nil)
            break
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        case .restricted:
            let alert = UIAlertController(title: "Aviso", message: "Restringido", preferredStyle: UIAlertController.Style.alert)
            let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
            alert.addAction(action)
            parent?.present(alert, animated: true, completion: nil)
            break
        case .authorizedAlways:
            break
        default:
            debugPrint("Desconocido")
        }
    }
    
}

extension WorkExperienceVC {
    
    var getContext: NSManagedObjectContext { return Bridge.context }
    
    // Métodos para almacenar información en las entidades del Core Data
    func insertJobData(model: JobPlaceModel) throws {
        let entity = JobEntity(context: getContext)
        
        entity.jobName = model.name
        entity.jobPosition = model.jobPosition
        entity.jobDetail = model.jobDetail
        entity.jobDate = model.jobDate
        entity.latitud = String(model.coordinate.latitude)
        entity.longitud = String(model.coordinate.longitude)
        
        getContext.insert(entity)
        try getContext.save()
    }
    
    // Método para extraer la información de la experiencia laboral de CoreData
    func fetchJobData() throws -> [JobPlaceModel] {
        var jsonArray = [JobPlaceModel]()
        let data = try getContext.fetch(JobEntity.fetchRequest() as NSFetchRequest<JobEntity>)
        
        for item in data {
            let model = JobPlaceModel(entity: item)
            jsonArray.append(model)
        }
        
        return jsonArray
    }
    
}
