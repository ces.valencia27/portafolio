//
//  ProjectsExtension.swift
//  Portafolio
//
//  Created by David Valencia on 9/11/19.
//  Copyright © 2019 David. All rights reserved.
//

import Foundation
import UIKit
import CoreData

extension ProjectsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    // MARK: - Métodos de protocolo
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return projectModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "project", for: indexPath) as? ProjectCell {
            
            cell.layer.masksToBounds = true
            cell.layer.cornerRadius = 5.0
            cell.backgroundColor = UIColor.customWhite
            
            let viewModel = ProjectViewModel(project: projectModels[indexPath.item])
            cell.modelView = viewModel
            
            return cell
        } else {
            
            return UICollectionViewCell()
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let wCell = interface.projectCollectionView.frame.width
        let hCell = interface.projectCollectionView.frame.width
        
        return CGSize(width: wCell, height: hCell)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        collectionView.cellForItem(at: indexPath)?.backgroundColor = UIColor.customBlue
        
        let viewModel = ProjectViewModel(project: projectModels[indexPath.item])
        interface.modelView = viewModel

    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        collectionView.cellForItem(at: indexPath)?.backgroundColor = UIColor.customWhite
    }
}

extension ProjectsVC: URL_SessionDelegate {

    // Métodos de protocolo URL_SessionDelegate
    func connectionFinishSuccessfull(session: URL_Session, response: NSDictionary) {
        ProjectModel.getDataModel(dict: response) { (jsonArray, respuesta) in
            
            if respuesta == true {
                projectModels = jsonArray ?? []
                interface.projectCollectionView.reloadData()
                
                DBManager.shared.cleanEntity(currentEntity: ProjectEntity.self)
                
                do {
                    for item in jsonArray ?? [] {
                        try insertProjectData(model: item)
                    }
                } catch let error {
                    debugPrint(error.localizedDescription)
                }
                
                guard let defaultValue = projectModels.first else {return}
                
                let viewModel = ProjectViewModel(project: defaultValue)
                interface.modelView = viewModel

            } else {
                debugPrint("No hay registros")
            }
            
            LoaderView.shared().hideActivityIndicator(uiView: self.view)
        }
    }
    
    func connectionFinishWithError(session: URL_Session, error: Error) {
        LoaderView.shared().hideActivityIndicator(uiView: self.view)
        
        self.navigationController?.present(AlertView.shared().showAlert(message: error.localizedDescription), animated: true, completion: nil)
    }
}

extension ProjectsVC {
    
    var getContext: NSManagedObjectContext { return Bridge.context }
    
    func insertProjectData(model: ProjectModel) throws {
        let entity = ProjectEntity(context: getContext)
        
        entity.image_name = model.image_name
        entity.project_name = model.project_name
        entity.project_description = model.project_description
        entity.link = model.link
        
        getContext.insert(entity)
        try getContext.save()
    }
    
    func fetchProjectData() throws -> [ProjectModel] {
        var jsonArray = [ProjectModel]()
        let data = try getContext.fetch(ProjectEntity.fetchRequest() as NSFetchRequest<ProjectEntity>)
        
        for item in data {
            let model = ProjectModel(entity: item)
            jsonArray.append(model)
        }
        
        return jsonArray
    }
}
