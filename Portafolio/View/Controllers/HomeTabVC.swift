//
//  HomeTabVC.swift
//  Portafolio
//
//  Created by David Valencia on 9/11/19.
//  Copyright © 2019 David. All rights reserved.
//

import UIKit

class HomeTabVC: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Configurando los componentes del controlador
        configNavBar()
        configTabBar()
    }
    
    // Método para configurar la navigationBar
    private func configNavBar() {
        
        let closeItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.stop, target: self, action: #selector(itemPressed(sender:)))
        closeItem.tag = 0
        navigationItem.leftBarButtonItem = closeItem
        
        let goalsItem = UIBarButtonItem(image: UIImage(named: "goalsIcon1"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(itemPressed(sender:)))
        goalsItem.tag = 1
        navigationItem.rightBarButtonItem = goalsItem
    }
    
    // Método para configurar la tabBar
    private func configTabBar() {
        
        let vc1 = PersonalInfoVC()
        let item1 = UITabBarItem(title: "Perfil", image: UIImage(named: "profileIcon"), tag: 0)
        vc1.tabBarItem = item1
        let vc2 = AcademyInfoVC()
        let item2 = UITabBarItem(title: "Estudios", image: UIImage(named: "schoolIcon"), tag: 1)
        vc2.tabBarItem = item2
        let vc3 = WorkExperienceVC()
        let item3 = UITabBarItem(title: "Experiencia", image: UIImage(named: "jobIcon"), tag: 2)
        vc3.tabBarItem = item3
        let vc4 = ProjectsVC()
        let item4 = UITabBarItem(title: "Proyectos", image: UIImage(named: "projectsIcon"), tag: 3)
        vc4.tabBarItem = item4
        
        self.setViewControllers([vc1, vc2, vc3, vc4], animated: true)
        self.tabBar.barTintColor = UIColor.clear
        self.tabBar.backgroundImage = UIImage()
        self.tabBar.shadowImage = UIImage()
        self.tabBar.tintColor = UIColor.customBlue
        self.tabBar.unselectedItemTintColor = UIColor.lightGray
        self.navigationItem.title = "PERFIL"
        self.navigationItem.rightBarButtonItem?.isEnabled = true
    }
    
    // Métodos de selector
    @objc private func itemPressed(sender: UIBarButtonItem) {
        
        if sender.tag != 0 {
            let modalVC = GoalsVC()
            let nav = UINavigationController(rootViewController: modalVC)
            nav.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            nav.navigationBar.shadowImage = UIImage()
            nav.modalPresentationStyle = .overCurrentContext
            nav.navigationBar.tintColor = UIColor.customBlue
            nav.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.customBlue, NSAttributedString.Key.font : UIFont(name: "DINCondensed-Bold", size: 25) ?? UIFont.systemFont(ofSize: 25, weight: UIFont.Weight.bold)]
            parent?.present(nav, animated: true, completion: nil)
        } else {
            dismiss(animated: true, completion: nil)
        }
    }
    
    // Métodos de protocolo
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        switch self.tabBar.selectedItem?.tag {
        case 0:
            self.navigationItem.title = "PERFIL"
            self.navigationItem.rightBarButtonItem?.isEnabled = true
        case 1:
            self.navigationItem.title = "FORMACIÓN ACADÉMICA"
            self.navigationItem.rightBarButtonItem?.isEnabled = false
        case 2:
            self.navigationItem.title = "EXPERIENCIA LABORAL"
            self.navigationItem.rightBarButtonItem?.isEnabled = false
        default:
            self.navigationItem.title = "PROYECTOS"
            self.navigationItem.rightBarButtonItem?.isEnabled = false
        }
    }

    // MARK: - Métodos de ciclo de vida del controlador
    override func viewWillDisappear(_ animated: Bool) {}
    
    override func viewDidAppear(_ animated: Bool) {}
    
    override func viewDidDisappear(_ animated: Bool) {}
    
    override func didReceiveMemoryWarning() {
        debugPrint("Problema de memoria")
    }

}
