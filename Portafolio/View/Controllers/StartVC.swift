//
//  StartVC.swift
//  Portafolio
//
//  Created by David Valencia on 9/11/19.
//  Copyright © 2019 David. All rights reserved.
//

import UIKit

class StartVC: UIViewController {
    
    let interface: StartView = {
        let interface = StartView(frame: CGRect.zero)
        interface.backgroundColor = UIColor.customWhite
        interface.translatesAutoresizingMaskIntoConstraints = false
        return interface
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Cargando la interfaz gráfica del controlador
        loadComponents()
    }
    
    // Método para cargar la interfaz gráfica del controlador
    private func loadComponents() {
        
        setSubviews()
        setAutolayout()
    }
    
    // Método para agregar la interfaz gráfica del controlador a la vista principal del controlador
    private func setSubviews() {
        
        view.addSubview(interface)
        interface.cvBtn.addTarget(self, action: #selector(cvPressed), for: UIControl.Event.touchUpInside)
    }
    
    @objc private func cvPressed() {
        let nextVC = HomeTabVC()
        let nav = UINavigationController(rootViewController: nextVC)
        nav.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        nav.navigationBar.shadowImage = UIImage()
        nav.navigationBar.tintColor = UIColor.customBlue
        nav.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.customBlue, NSAttributedString.Key.font : UIFont(name: "DINCondensed-Bold", size: 25) ?? UIFont.systemFont(ofSize: 25, weight: UIFont.Weight.bold)]
        present(nav, animated: true, completion: nil)

//        LoaderView.shared().showActivityIndicatory(uiView: self.view)
//
//        let networkManager = URL_Session()
//        networkManager.delegate = self
//
//        let queue = DispatchQueue(label: "queue", attributes: .concurrent)
//        let semaphore = DispatchSemaphore(value: 1)
//
//        queue.async {
//            networkManager.getRequest(endpoint: Urls.shared().personalInfoURL)
//            semaphore.wait()
//            sleep(1)
//            semaphore.signal()
//            networkManager.getRequest(endpoint: Urls.shared().academyURL)
//            semaphore.wait()
//            sleep(1)
//            networkManager.getRequest(endpoint: Urls.shared().goalsURL)
//            semaphore.signal()
//            DispatchQueue.main.async {
//                LoaderView.shared().hideActivityIndicator(uiView: self.view)
//            }
//        }

    }
    
    // Método para definir el autolayout de la interfaz gráfica del controlador
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            interface.topAnchor.constraint(equalTo: view.topAnchor),
            interface.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            interface.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            interface.leadingAnchor.constraint(equalTo: view.leadingAnchor)
            ])
    }
    
    // MARK: - Métodos de ciclo del vida del controlador
    override func viewWillAppear(_ animated: Bool) {}
    
    override func viewWillDisappear(_ animated: Bool) {}
    
    override func viewDidAppear(_ animated: Bool) {}
    
    override func viewDidDisappear(_ animated: Bool) {}
    
    override func didReceiveMemoryWarning() {
        debugPrint("Problema de memoria")
    }
}
