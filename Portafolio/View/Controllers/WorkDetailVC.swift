//
//  WorkDetailVC.swift
//  Portafolio
//
//  Created by David Valencia on 9/11/19.
//  Copyright © 2019 David. All rights reserved.
//

import UIKit

class WorkDetailVC: UIViewController {

    let interface: WorkDetailView = {
        let interface = WorkDetailView(frame: CGRect.zero)
        interface.backgroundColor = UIColor.white
        interface.translatesAutoresizingMaskIntoConstraints = false
        return interface
    }()
    
    var currentJob: JobPlaceModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Cargando la interfaz gráfica del controlador
        setSubviews()
        setAutolayout()
        updateUI()
    }
    
    // Método para agregar la interfaz gráfica a la vista principal del controlador
    private func setSubviews() {
        
        self.view.addSubview(interface)
    }
    
    // Método para definir el autolayout de la interfaz gráfica
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            interface.topAnchor.constraint(equalTo: self.view.topAnchor),
            interface.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            interface.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            interface.leadingAnchor.constraint(equalTo: self.view.leadingAnchor)
            ])
    }
    
    // Método para recibir la información por medio del protocolo
    private func updateUI() {
        
        if let job = currentJob {
            let viewModel = JobPlaceViewModel(job: job)
            interface.modelView = viewModel
        }
    }
    
    // MARK: - Métodos de ciclo de vida del controlador
    override func viewWillAppear(_ animated: Bool) {}
    
    override func viewWillDisappear(_ animated: Bool) {}
    
    override func viewDidAppear(_ animated: Bool) {}
    
    override func viewDidDisappear(_ animated: Bool) {}
    
    override func didReceiveMemoryWarning() {
        debugPrint("Problema de memoria")
    }
    
}
