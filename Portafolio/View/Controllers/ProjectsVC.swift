//
//  ProjectsVC.swift
//  Portafolio
//
//  Created by David Valencia on 9/11/19.
//  Copyright © 2019 David. All rights reserved.
//

import UIKit

class ProjectsVC: UIViewController {

    let interface: ProjectsView = {
        let interface = ProjectsView(frame: CGRect.zero)
        interface.backgroundColor = UIColor.customWhite
        interface.translatesAutoresizingMaskIntoConstraints = false
        return interface
    }()
    
    var projectModels = [ProjectModel]()
    var networkManager: URL_Session?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Cargando la interfaz gráfica del controlador
        loadComponents()
    }
    
    // Método para configurar la interfaz gráfica del controlador
    private func loadComponents() {
        
        setDelegates()
        setSubviews()
        setAutolayout()
        validateConnection()
        

    }
    
    // Método para agregar la interfaz gráfica a la vista principal del controlador
    private func setSubviews() {
        
        view.backgroundColor = UIColor.customWhite
        self.view.addSubview(interface)
    }
    
    // Método para definir el autolayout de la interfaz gráfica del controlador
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            interface.topAnchor.constraint(equalTo: self.view.topAnchor),
            interface.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            interface.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            interface.leadingAnchor.constraint(equalTo: self.view.leadingAnchor)
            ])
    }
    
    // Método para asignar los objetos que actuarán como delegados y dataSource
    private func setDelegates() {
        
        interface.projectCollectionView.delegate = self
        interface.projectCollectionView.dataSource = self
    }
    
    // Método para hacer la petición y obtener la información de los trabajos anteriores
    private func request() {
        
        networkManager = URL_Session()
        networkManager?.delegate = self
        networkManager?.getRequest(endpoint: Endpoints.shared().projects)
        LoaderView.shared().showActivityIndicatory(uiView: self.view)
    }
    
    // Método para validar si existe conexión a internet y hacer la petición o extraer la información del coreData
    private func validateConnection() {
        
        if NetworkConnection.isConnectedToNetwork() {
            request()
        } else {
            do {
                let projectData = try fetchProjectData()
                projectModels.removeAll()
                projectModels = projectData
                interface.projectCollectionView.reloadData()
                
                guard let defaultValue = projectModels.first else {return}
                let viewModel = ProjectViewModel(project: defaultValue)
                interface.modelView = viewModel
                
            } catch let error {
                debugPrint(error.localizedDescription)
            }
        
        }
    }

    // MARK: - Métodos de ciclo de vida del controlador
    override func viewWillAppear(_ animated: Bool) {}
    
    override func viewWillDisappear(_ animated: Bool) {}
    
    override func viewDidAppear(_ animated: Bool) {}
    
    override func viewDidDisappear(_ animated: Bool) {}
    
    override func didReceiveMemoryWarning() {
        debugPrint("Problema de memoria")
    }
}
