//
//  PersonalInfoVC.swift
//  Portafolio
//
//  Created by David Valencia on 9/11/19.
//  Copyright © 2019 David. All rights reserved.
//

import UIKit

class PersonalInfoVC: UIViewController {
    
    let interface: PersonalInfoView = {
        let interface = PersonalInfoView(frame: CGRect.zero)
        interface.backgroundColor = UIColor.white
        interface.translatesAutoresizingMaskIntoConstraints = false
        return interface
    }()
    
    private let urlsArray = [Endpoints.shared().personal_info, Endpoints.shared().skills]
    var skillsArray = [SkillsModel]()
    var filterArray = [SkillsModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Cargando la interfaz gráfica del controlador
        loadComponents()
    }
    
    // Método para configurar la interfaz gráfica del controlador
    private func loadComponents() {
        
        setDelegates()
        setSegmentedTargets()
        setSubviews()
        setAutolayout()
        validateConnection()
    }
    
    // Método para agregar la interfaz gráfica a la vista principal del controlador
    private func setSubviews() {
        
        self.view.addSubview(interface)
    }
    
    // Método para definir el autolayout de la interfaz gráfica del controlador
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            interface.topAnchor.constraint(equalTo: self.view.topAnchor),
            interface.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            interface.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            interface.leadingAnchor.constraint(equalTo: self.view.leadingAnchor)
            ])
    }
    
    // Método para asignar los objetos que actuarán como delegados y dataSource
    private func setDelegates() {
        
        interface.skillsTable.delegate = self
        interface.skillsTable.dataSource = self
    }
    
    // Método para asignar los eventos del segmentedControl
    private func setSegmentedTargets() {
        
        interface.segmented.addTarget(self, action: #selector(segmentedChanged(sender:)), for: UIControl.Event.valueChanged)
    }
    
    @objc private func segmentedChanged(sender: UISegmentedControl) {
        
        if sender.selectedSegmentIndex != 0 {
            filterArray.removeAll()
            filterArray = skillsArray.filter({$0.skillType == "personal"})
            self.interface.skillsTable.reloadData()
        } else {
            filterArray.removeAll()
            filterArray = skillsArray.filter({$0.skillType == "profesional"})
            self.interface.skillsTable.reloadData()
        }
        
    }
    
    // Método que inicia el semáforo para el manejo de las peticiones
    private func request() {
        
        LoaderView.shared().showActivityIndicatory(uiView: self.tabBarController!.view)

        let queue = OperationQueue()
        queue.maxConcurrentOperationCount = 2
        queue.waitUntilAllOperationsAreFinished()
        
        for endpoint in urlsArray {
            queue.addOperation {
                let networkManager = URL_Session()
                networkManager.delegate = self
                networkManager.getRequest(endpoint: endpoint)
            }
        }
    
    }
    
    // Método para validar si existe conexión a internet y hacer la petición o extraer la información del coreData
    private func validateConnection() {
        
        if NetworkConnection.isConnectedToNetwork() {
            request()
        } else {
            do {
                let data = try fetchPersonalInfo()
                let viewModel = PersonalInfoViewModel(info: data)
                interface.modelView = viewModel
            } catch let error {
                debugPrint(error.localizedDescription)
            }
            
            do {
                let data = try fetchSkills()
                skillsArray = data
                filterArray.removeAll()
                filterArray = skillsArray.filter({$0.skillType == "profesional"})
                interface.skillsTable.reloadData()
            } catch let error {
                debugPrint(error.localizedDescription)
            }
        }
    }
    
    // MARK: - Métodos de ciclo de vida del controlador
    override func viewWillAppear(_ animated: Bool) {}
    
    override func viewWillDisappear(_ animated: Bool) {}
    
    override func viewDidAppear(_ animated: Bool) {}
    
    override func viewDidDisappear(_ animated: Bool) {}
    
    override func didReceiveMemoryWarning() {}
}
