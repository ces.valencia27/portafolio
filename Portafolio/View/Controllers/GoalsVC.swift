//
//  GoalsVC.swift
//  Portafolio
//
//  Created by David Valencia on 9/11/19.
//  Copyright © 2019 David. All rights reserved.
//

import UIKit

class GoalsVC: UIViewController {
    
    let interface: GoalsView = {
        let interface = GoalsView(frame: CGRect.zero)
        interface.translatesAutoresizingMaskIntoConstraints = false
        return interface
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Cargando la interfaz gráfica del controlador
        loadComponents()
    }
    
    // Método para cargar la interfaz gráfica del controlador
    private func loadComponents() {
        
        configNavBar()
        setSubviews()
        setAutolayout()
        validateConnection()
    }
    
    // Método para configurar la navigationBar
    private func configNavBar() {
        
        self.title = "Objetivos Profesionales"
        let closeItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.stop, target: self, action: #selector(itemPressed))
        navigationItem.leftBarButtonItem = closeItem
        
    }
    
    // Método para agregar la interfaz gráfica a la vista principal del controlador
    private func setSubviews() {
        
        view.backgroundColor = UIColor.clear
        view.addSubview(interface)
    }
    
    // Método para definir el autolayout de la interfaz gráfica del controlador
    private func setAutolayout() {
        
        NSLayoutConstraint.activate([
            interface.topAnchor.constraint(equalTo: self.view.topAnchor),
            interface.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            interface.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            interface.leadingAnchor.constraint(equalTo: self.view.leadingAnchor)
            ])
    }
    
    // Métodos de selector
    @objc private func itemPressed() {
        
        dismiss(animated: true, completion: nil)
    }
    
    // Método para hacer la petición y obtener la información de los objetivos profesionales
    private func request() {
        
        let networkManager = URL_Session()
        networkManager.delegate = self
        networkManager.getRequest(endpoint: Endpoints.shared().goals)
        LoaderView.shared().showActivityIndicatory(uiView: self.view)
    }
    
    // Método para validar si existe conexión a internet y hacer la petición o extraer la información del coreData
    private func validateConnection() {
        
        if NetworkConnection.isConnectedToNetwork() {
            request()
        } else {
            do {
                let dataModel = try fetchGoalsData()
                let viewModel = GoalsViewModel(goals: dataModel)
                interface.modelView = viewModel
            } catch let error {
                debugPrint(error.localizedDescription)
            }
        }
    }
    
    // MARK: - Métodos de ciclo de vida del controlador
    override func viewWillAppear(_ animated: Bool) {}
    
    override func viewWillDisappear(_ animated: Bool) {}
    
    override func viewDidAppear(_ animated: Bool) {}
    
    override func viewDidDisappear(_ animated: Bool) {}
    
    override func didReceiveMemoryWarning() {
        debugPrint("Problema de memoria")
    }
    
}
