//
//  SchoolViewModel.swift
//  Portafolio
//
//  Created by David Valencia on 9/11/19.
//  Copyright © 2019 David. All rights reserved.
//

import Foundation
import UIKit

struct SchoolViewModel {
    
    private let school: SchoolModel
    
    init(school: SchoolModel) {
        self.school = school
    }
    
    public var getName: String { return school.schoolName }
    public var getStudies: String { return school.estudies }
    public var getStartDate: String { return school.fecha_inicio }
    public var getFinishDate: String { return school.fecha_final }
    public var getImageName: String { return school.image_name }
}
