//
//  GoalsViewModel.swift
//  Portafolio
//
//  Created by David Valencia on 9/11/19.
//  Copyright © 2019 David. All rights reserved.
//

import Foundation
import UIKit

struct GoalsViewModel {
    
    private let goalsDesc: GoalsModel
    
    init(goals: GoalsModel) {
        self.goalsDesc = goals
    }
    
    public var getGoals: String { return goalsDesc.goals }
}

