//
//  PersonalInfoViewModel.swift
//  Portafolio
//
//  Created by David Valencia on 9/11/19.
//  Copyright © 2019 David. All rights reserved.
//

import Foundation
import UIKit

struct PersonalInfoViewModel {
    
    private let info: PersonalInfoModel
    
    init(info: PersonalInfoModel) {
        self.info = info
    }
    
    public var getName: String { return info.name }
    public var getPhone: String { return info.phone }
    public var getMail: String { return info.mail }
    public var getPosition: String { return info.position }
    
}


