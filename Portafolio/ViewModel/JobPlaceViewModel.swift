//
//  JobPlaceViewModel.swift
//  Portafolio
//
//  Created by David Valencia on 9/11/19.
//  Copyright © 2019 David. All rights reserved.
//


import Foundation
import UIKit

struct JobPlaceViewModel {
    
    private let job: JobPlaceModel
    
    init(job: JobPlaceModel) {
        self.job = job
    }
    
    public var getName: String { return job.name ?? "" }
    public var getPosition: String { return job.jobPosition ?? "" }
    public var getDetail: String { return job.jobDetail ?? "" }
    public var getDate: String { return job.jobDate ?? "" }
}
