//
//  SkillsViewModel.swift
//  Portafolio
//
//  Created by David Valencia on 9/11/19.
//  Copyright © 2019 David. All rights reserved.
//

import Foundation
import UIKit

struct SkillsViewModel {
    
    private let skillModel: SkillsModel
    
    init(skillModel: SkillsModel) {
        self.skillModel = skillModel
    }
    
    public var getSkill: String { return skillModel.skillName }
    public var getSkillType: String { return skillModel.skillType }
}
