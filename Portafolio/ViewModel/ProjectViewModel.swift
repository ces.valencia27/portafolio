//
//  ProjectViewModel.swift
//  Portafolio
//
//  Created by David Valencia on 9/11/19.
//  Copyright © 2019 David. All rights reserved.
//

import Foundation
import UIKit

struct ProjectViewModel {
    
    private let project: ProjectModel
    
    init(project: ProjectModel) {
        self.project = project
    }
    
    public var getName: String { return project.project_name }
    public var getDescription: String { return project.project_description }
    public var getImageName: String { return project.image_name }
    public var getLink: String { return project.link }

}
