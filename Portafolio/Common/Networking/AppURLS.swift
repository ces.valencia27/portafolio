//
//  AppURLS.swift
//  Portafolio
//
//  Created by David Valencia on 9/11/19.
//  Copyright © 2019 David. All rights reserved.
//

import Foundation

public enum Environment {

    private static let infoDictionary: [String: Any] = {
        guard let dict = Bundle.main.infoDictionary else {
            fatalError("El archivo con extensión .plist no fue encontrado")
        }
        return dict
    }()
    
    static let baseURL: URL = {
        guard let rootURLstring = Environment.infoDictionary["ROOT_URL"] as? String else {
            fatalError("La URL Base no existe en el archivo info.plist")
        }
        guard let url = URL(string: rootURLstring) else {
            fatalError("La URL es inválida")
        }
        return url
    }()
}

import Foundation

class Endpoints {

    private static let sharedConstant = Endpoints()

    let personal_info: String = "/personal_info.json"
    let skills: String = "/skills.json"
    let academy: String = "/schools.json"
    let work_experience: String = "/work_experience.json"
    let projects: String = "/projects.json"
    let goals: String = "/goals.json"

    public static func shared() -> Endpoints { return sharedConstant }
}

