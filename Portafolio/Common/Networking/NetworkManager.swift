//
//  NetworkManager.swift
//  Portafolio
//
//  Created by David Valencia on 9/11/19.
//  Copyright © 2019 David. All rights reserved.
//

import Foundation
import UIKit

protocol URL_SessionDelegate: class {
    
    func connectionFinishSuccessfull(session: URL_Session, response: NSDictionary)
    func connectionFinishWithError(session: URL_Session, error: Error)
}

class URL_Session: NSObject, URLSessionDelegate, URLSessionDataDelegate {
    
    var dataTask: URLSessionDataTask?
    var responseData: Data = Data()
    var httpResponse: HTTPURLResponse?
    weak var delegate: URL_SessionDelegate?
    
    override init() {
        super.init()
    }
    
    // Método GET para obtener la información de los archivos json montados en github
    func getRequest(endpoint: String) {
        
        if dataTask != nil {
            dataTask?.cancel()
        }
        
        let urlString = Environment.baseURL.absoluteString + endpoint
        let urlComponents = NSURLComponents(string: urlString)
        let sessionCofiguration = URLSessionConfiguration.default
        let defaultSession = URLSession(configuration: sessionCofiguration, delegate: self, delegateQueue: OperationQueue.main)
        
        
        if let urlComp = urlComponents {
            
            var request = URLRequest(url: urlComp.url!)
            request.httpMethod = "GET"
            request.timeoutInterval = 60
            responseData = Data()
            dataTask = defaultSession.dataTask(with: request)
            dataTask?.resume()
        } else {
            debugPrint("Error en el desempaquetado")
        }
        
    }
    
    // Método que evalúa si el task finalizó con éxito o error
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        
        if error == nil {
            if let resultado = try? JSONSerialization.jsonObject(with: responseData, options: .mutableContainers) as? NSDictionary {
                
                delegate?.connectionFinishSuccessfull(session: self, response: resultado)
            } else {
                debugPrint("Ocurrió un error en la serialización del JSON")
            }
        } else {
            delegate?.connectionFinishWithError(session: self, error: error!)
        }
    }
    
    // Método que descarga la representación de bytes
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        responseData.append(data)
    }
    
    // Método que cacha la respuesta del servidor
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Void) {
        
        httpResponse = response as? HTTPURLResponse
        
        if httpResponse?.statusCode == 200 {
            completionHandler(URLSession.ResponseDisposition.allow)
        } else {
            completionHandler(URLSession.ResponseDisposition.cancel)
        }
        
    }
    
}



