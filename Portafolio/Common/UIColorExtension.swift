//
//  UIColorExtension.swift
//  Portafolio
//
//  Created by David Valencia on 9/11/19.
//  Copyright © 2019 David. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    static var customWhite: UIColor = UIColor(red: 242/255, green: 242/255, blue: 242/255, alpha: 1)
    static var customBlue: UIColor = UIColor(red: 53/255, green: 59/255, blue: 80/255, alpha: 1)
}

