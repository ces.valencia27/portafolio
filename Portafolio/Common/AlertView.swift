//
//  AlertView.swift
//  Portafolio
//
//  Created by David Valencia on 9/11/19.
//  Copyright © 2019 David. All rights reserved.
//

import Foundation
import UIKit

class AlertView {
    
    static let sharedProperty = AlertView()
    
    // Método que muestra un activity indicator
    func showAlert(message: String) -> UIAlertController {
        
        let alert = UIAlertController(title: "Mensaje", message: message, preferredStyle: UIAlertController.Style.alert)
        let action = UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil)
        alert.addAction(action)
        
        return alert
    }
    
    // Método que devuelve una instancia de la clase
    static func shared() -> AlertView { return sharedProperty }
}


