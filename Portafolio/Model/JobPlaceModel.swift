//
//  JobPlaceModel.swift
//  Portafolio
//
//  Created by David Valencia on 9/11/19.
//  Copyright © 2019 David. All rights reserved.
//

import Foundation
import MapKit

class JobPlaceModel: NSObject, MKAnnotation {
    
    var title: String? = ""
    var locationName: String = ""
    var jobPosition: String? = ""
    var jobDate: String? = ""
    var jobDetail: String? = ""
    var coordinate: CLLocationCoordinate2D = CLLocationCoordinate2D()
    
    override init() {
        
    }
    
    init(title: String, locationName: String, jobPosition: String, jobDate: String, jobDetail: String?, coordinate: CLLocationCoordinate2D) {
        
        self.title = title
        self.locationName = locationName
        self.jobPosition = jobPosition
        self.jobDate = jobDate
        self.jobDetail = jobDetail
        self.coordinate = coordinate
        
        super.init()
    }
    
    init(entity: JobEntity) {
        
        let latitud = Double(entity.latitud ?? "")
        let longitud = Double(entity.longitud ?? "")
        
        self.title = entity.jobName ?? ""
        self.locationName = entity.jobName ?? ""
        self.jobPosition = entity.jobPosition
        self.jobDate = entity.jobDate
        self.jobDetail = entity.jobDetail
        self.coordinate = CLLocationCoordinate2D(latitude: latitud ?? 0.0, longitude: longitud ?? 0.0)
    }
    
    var name: String? {
        return locationName
    }
}

extension JobPlaceModel {
    
    static func getDataModel(dict: NSDictionary, completion: ([JobPlaceModel]?, Bool) -> Void) {
        
        var jobArray = [JobPlaceModel]()
        if dict.count > 0 {
            let jsonArray = dict["jobs"] as? [NSDictionary] ?? [[:]]
            
            for item in jsonArray {
                let jobName = item["jobName"] as? String ?? ""
                let jobDate = item["jobDate"] as? String ?? ""
                let jobPosition = item["jobPosition"] as? String ?? ""
                let jobDetail = item["jobDetail"] as? String ?? ""
                let latitud = item["latitud"] as? String ?? ""
                let longitud = item["longitud"] as? String ?? ""
                let doubleLat = Double(latitud) ?? 0.0
                let doubleLon = Double(longitud) ?? 0.0
                
                let coordinate = CLLocationCoordinate2D(latitude: doubleLat, longitude: doubleLon)
                
                let object = JobPlaceModel(title: jobName, locationName: jobName, jobPosition: jobPosition, jobDate: jobDate, jobDetail: jobDetail, coordinate: coordinate)
                
                jobArray.append(object)
            }
            completion(jobArray, true)
        } else {
            completion(nil, false)
        }
    }
}
