//
//  SchoolModel.swift
//  Portafolio
//
//  Created by David Valencia on 9/11/19.
//  Copyright © 2019 David. All rights reserved.
//

import Foundation

struct SchoolModel {
    
    let schoolName: String
    let estudies: String
    let fecha_inicio: String
    let fecha_final: String
    let image_name: String
    
    init(dictionary: NSDictionary) {
        
        self.schoolName = dictionary["schoolName"] as? String ?? ""
        self.estudies = dictionary["estudies"] as? String ?? ""
        self.fecha_inicio = dictionary["fecha_inicio"] as? String ?? ""
        self.fecha_final = dictionary["fecha_final"] as? String ?? ""
        self.image_name = dictionary["image_name"] as? String ?? ""
    }
    
    init(entity: SchoolEntity) {
        
        self.schoolName = entity.schoolName ?? ""
        self.estudies = entity.estudies ?? ""
        self.fecha_inicio = entity.fecha_inicio ?? ""
        self.fecha_final = entity.fecha_final ?? ""
        self.image_name = entity.image_name ?? ""
    }
    
}

extension SchoolModel {
    
    static func getDataModel(dict: NSDictionary, completion: ([SchoolModel]?, Bool) -> Void) {
        
        var schoolArray = [SchoolModel]()
        if dict.count > 0 {
            let jsonArray = dict["schools"] as? [NSDictionary] ?? [[:]]
            
            for item in jsonArray {
                let object = SchoolModel(dictionary: item)
                schoolArray.append(object)
            }
            completion(schoolArray, true)
        } else {
            completion(nil, false)
        }
    }
}
