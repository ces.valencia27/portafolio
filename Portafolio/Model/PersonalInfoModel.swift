//
//  PersonalInfoModel.swift
//  Portafolio
//
//  Created by David Valencia on 9/11/19.
//  Copyright © 2019 David. All rights reserved.
//

import Foundation

struct PersonalInfoModel {
    
    let name: String
    let phone: String
    let mail: String
    let position: String
    
    init(dictionary: NSDictionary) {
        
        self.name = dictionary["name"] as? String ?? ""
        self.phone = dictionary["phone"] as? String ?? ""
        self.mail = dictionary["mail"] as? String ?? ""
        self.position = dictionary["position"] as? String ?? ""
    }
    
    init(entity: PersonalInfoEntity) {
        
        self.name = entity.name ?? ""
        self.position = entity.position ?? ""
        self.mail = entity.mail ?? ""
        self.phone = entity.phone_number ?? ""
    }
    
}

extension PersonalInfoModel {
    
    static func getDataModel(dict: NSDictionary) -> PersonalInfoModel? {
        
        if let mainResponse = dict["personal_info"] as? NSDictionary {
            let object = PersonalInfoModel(dictionary: mainResponse)
            
            return object
        } else {
            return nil
        }
    }
}
