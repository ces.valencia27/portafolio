//
//  ProjectModel.swift
//  Portafolio
//
//  Created by David Valencia on 9/11/19.
//  Copyright © 2019 David. All rights reserved.
//

import Foundation

struct ProjectModel {
    
    let project_name: String
    let project_description: String
    let image_name: String
    let link: String
    
    init(dictionary: NSDictionary) {
        
        self.project_name = dictionary["project_name"] as? String ?? ""
        self.project_description = dictionary["project_description"] as? String ?? ""
        self.image_name = dictionary["image_name"] as? String ?? ""
        self.link = dictionary["link"] as? String ?? ""
    }
    
    init(entity: ProjectEntity) {
        
        self.project_name = entity.project_name ?? ""
        self.project_description = entity.project_description ?? ""
        self.link = entity.link ?? ""
        self.image_name = entity.image_name ?? ""
    }
    
}

extension ProjectModel {
    
    static func getDataModel(dict: NSDictionary, completion: ([ProjectModel]?, Bool) -> Void) {
        
        var projectArray = [ProjectModel]()
        if dict.count > 0 {
            let jsonArray = dict["projects"] as? [NSDictionary] ?? [[:]]
            
            for item in jsonArray {
                let object = ProjectModel(dictionary: item)
                projectArray.append(object)
            }
            completion(projectArray, true)
        } else {
            completion(nil, false)
        }
    }
}
