//
//  GoalsModel.swift
//  Portafolio
//
//  Created by David Valencia on 9/11/19.
//  Copyright © 2019 David. All rights reserved.
//

import Foundation

struct GoalsModel {
    
    let goals: String
    
    init(dictionary: NSDictionary) {
        
        self.goals = dictionary["goals"] as? String ?? ""
    }
    
    init(entity: GoalsEntity) {
        
        self.goals = entity.goals ?? ""
    }
}

