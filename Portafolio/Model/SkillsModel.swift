//
//  SkillsModel.swift
//  Portafolio
//
//  Created by David Valencia on 9/11/19.
//  Copyright © 2019 David. All rights reserved.
//

import Foundation

struct SkillsModel {
    
    let skillName: String
    let skillType: String
    
    init(dictionary: NSDictionary) {
        
        self.skillName = dictionary["skill"] as? String ?? ""
        self.skillType = dictionary["type"] as? String ?? ""
    }
    
    init(entity: SkillEntity) {
        
        self.skillName = entity.skill_name ?? ""
        self.skillType = entity.skill_type ?? ""
    }
}

extension SkillsModel {
    
    static func getPersonalSkills(dict: NSDictionary, completion: ([SkillsModel]?, Bool) -> Void) {
        
        var skills = [SkillsModel]()
        if dict.count > 0 {
            let jsonArray = dict["skills"] as? [NSDictionary] ?? [[:]]
            
            for item in jsonArray {
                let object = SkillsModel(dictionary: item)
                skills.append(object)
            }
            completion(skills, true)
        } else {
            completion(nil, false)
        }
    }
    
}
